extends KinematicBody2D
class_name Player

const MASK_BIT = 1

export var normal_speed = 60
export var thief_speed = 120
export var gravity = 480
export var jump_impulse = 360
export var jump_max_speed_x = 180
export var jump_accel_x = 360
export var jump_drop_speed = 640
export var jump_peak_max_speed_y = 60

var velocity = Vector2()
var thief_mode = false setget set_thief_mode
var interactable_objects = {}
var last_door_entered
var loot = 0
var has_key = false
var won = false
onready var on_floor = is_on_floor()

func _ready():
	change_to_room(get_parent())

func set_thief_mode(tm):
	thief_mode = tm
	var tilemap_color = Color.darkgray if tm else Color.white
	get_tree().call_group("TileMap", "set_modulate", tilemap_color)
	$ThiefStreamPlayer2D.play()

func face(dir):
	if dir < 0:
		$Sprite.scale.x = -1
	elif dir > 0:
		$Sprite.scale.x = 1

func _process(delta):
	var interactable_object
	for object in interactable_objects:
		if object and object.is_now_interactable():
			interactable_object = object
			break
	$InteractInstruction.visible = false
	if interactable_object:
		var interaction_name = interactable_object.get_interaction_name()
		var is_theft = interactable_object.interaction_is_theft()
		if Input.is_action_just_pressed("ui_up"):
			if !thief_mode and is_theft:
				self.thief_mode = true
			interactable_object.call(interaction_name, self)
		else:
			$InteractInstruction.visible = true
			$InteractInstruction.modulate = Color.red if is_theft else Color.white
			$InteractInstruction.text = "Up : " + interaction_name

	var move_x = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	if won:
		move_x = -1
		$Sprite.modulate.a = move_toward($Sprite.modulate.a, 0, delta)
	face(move_x)

	var animation = $AnimationPlayer.current_animation
	if on_floor:
		if Input.is_action_just_pressed("ui_select"):
			self.thief_mode = !thief_mode
		velocity.y = 0
		var speed = thief_speed if thief_mode else normal_speed
		if Input.is_action_just_pressed("ui_accept"):
			if !thief_mode:
				self.thief_mode = true
			velocity.y = -jump_impulse
			speed = jump_max_speed_x
		var velx = move_x * speed
		velocity.x = velx
		if move_x != 0:
			animation = "walk"
		else:
			animation = "stand"
		if thief_mode:
			animation = "thief_" + animation
		$AnimationPlayer.play(animation)
	else:
		if !thief_mode:
			self.thief_mode = true
		if Input.is_action_just_pressed("ui_down"):
			velocity.x = move_x * jump_max_speed_x
			velocity.y = jump_drop_speed
			$AnimationPlayer.play("quick_drop")
		if velocity.y < jump_drop_speed:
			if velocity.y < -jump_peak_max_speed_y:
				$AnimationPlayer.play("jump_up")
			elif velocity.y > jump_peak_max_speed_y:
				$AnimationPlayer.play("jump_down")
			else:
				$AnimationPlayer.play("jump_peak")

func _physics_process(delta):
	var move_x = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	velocity.x += move_x * jump_accel_x * delta
	velocity.x = max(-jump_max_speed_x, min(velocity.x, jump_max_speed_x))
	velocity.y += gravity * delta
	velocity.y = min(velocity.y, jump_drop_speed)
	velocity = move_and_slide(velocity, Vector2.UP)
	on_floor = is_on_floor()

func add_interactable(object:Interactable):
	interactable_objects[object] = object

func remove_interactable(object):
	interactable_objects.erase(object)

func change_to_room(room):
	if get_parent() != room:
		get_parent().remove_child(self)
		room.add_child(self)
	var camera = room.get_node("Camera2D")
	if camera:
		camera.current = true

func go_to_door(door:Door):
	change_to_room(door.get_parent())
	position = door.position
	last_door_entered = door.get_node(door.dest_door)

const MONEY_SOUNDS = [
	preload("res://sfx/loot1.ogg"),
	preload("res://sfx/loot2.ogg")
]

func add_money(money):
	loot += money
	var popup = PopupFactory.instance(get_parent())
	popup.global_position = $InteractInstruction.rect_global_position + $InteractInstruction.rect_size/2
	popup.velocity.y = -30
	popup.set_popup("$"+str(money), 1.0, Color.green)
	$AudioStreamPlayer2D.stream = MONEY_SOUNDS[0] if money < 1000 else MONEY_SOUNDS[1]
	$AudioStreamPlayer2D.play()

func take_key():
	has_key = true

func take_loot(loot):
	loot.get_parent().remove_child(loot)
	$Sprite.add_child(loot)
	loot.show_behind_parent = false
	loot.position = $Sprite.position

func _on_exit_area_entered(exit:Area2D):
	if $Sprite.has_node("Gem"):
		won = true
		var victory = get_parent().get_node_or_null("Victory")
		if victory:
			victory.visible = true
