extends Interactable
class_name Door

const NEXT_MASK_BIT = 4
const PREV_MASK_BIT = 8

export var dest_door:NodePath

func is_locked():
	var locked_door = get_node_or_null("LockedDoor")
	return locked_door and locked_door.visible
	
func unlock():
	var locked_door = get_node_or_null("LockedDoor")
	if locked_door:
		locked_door.visible = false

func _ready():
	Interactable_ready()

func is_now_interactable():
	return true

func get_interaction_name():
	return "enter"

func enter(user):
	if not has_node(dest_door):
		var popup = preload("res://Popup.tscn").instance()
		add_child(popup)
		popup.position = Vector2()
		popup.velocity.y = -60
		popup.set_popup("ERROR: Doesn't go anywhere", 1.0, Color.red)
		return
	if is_locked():
		if user.get("has_key") == true:
			unlock()
		else:
			var popup = preload("res://Popup.tscn").instance()
			add_child(popup)
			popup.position = Vector2()
			popup.velocity.y = -60
			popup.set_popup("LOCKED", 1.0, Color.white)
			return
	if user.has_method("go_to_door"):
		var door = get_node(dest_door)
		user.go_to_door(door)
