extends Node2D

export var velocity = Vector2()

func _process(delta):
	position += velocity*delta

func _on_Timer_timeout():
	queue_free()

func set_popup(text, time, color):
	$Label.text = text
	modulate = color
	$Timer.start(time)
