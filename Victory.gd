extends Label

var hue = 0.0

func _process(delta):
	modulate = Color.from_hsv(hue, 1.0, 1.0)
	hue += delta*8
	hue -= floor(hue)
