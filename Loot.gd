extends Interactable
class_name Loot

export var value = 1000
export var alerts_all = false

var taken = false

func _ready():
	Interactable_ready()

func is_now_interactable():
	return !taken

func get_interaction_name():
	return "steal"

func steal(thief):
	taken = true
	thief.add_money(value)
	thief.take_loot(self)
	if alerts_all:
		get_tree().call_group("NPC", "alert")
		$AudioStreamPlayer2D.play()
	get_tree().call_group("Tutorial", "set_visible", false)
