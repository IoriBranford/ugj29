extends Interactable
class_name NPC

const MASK_BIT = 2

enum Behavior { patrol, flee, chase, search }
var BEHAVIOR_NAMES = Behavior.keys()

export (Behavior) var initial_behavior = Behavior.patrol
export (Behavior) var alert_behavior = Behavior.flee

export(int,-1,1,2) var initial_face = 1
export var alert_speed = 180
export var blind = false
export var patrol_speed = 60
export var patrol_distance = 128
export var patrol_stop_time = 3.0
export var patrol_turn_speed = 2
export var pickpocket_value = 100
export var pickpocket_key = false
export var patrol_lines:PoolStringArray
export var patrol_line_time = 1.0
export(float, 0.0, 1.0) var patrol_line_chance = 0.5
export var patrol_enter_door_speed = 1
export var alert_turn_speed = 4
export var alert_enter_door_speed = 4
export var human_alert = false

onready var behavior = initial_behavior
var routine = null
var alerted = false
var last_dest_door

func _ready():
	$Sprite/LookRay.enabled = !blind
	Interactable_ready()
	$Sprite.scale.x = initial_face

func say_random(lines, time, chance, color = Color.white):
	if lines.size() > 0 && chance >= randf():
		say(lines[randi() % lines.size()], time, color)

func patrol():
	$Sprite.play("move")
	var destx = global_position.x + $Sprite.scale.x * patrol_distance
	while global_position.x != destx:
		var movex = yield()*patrol_speed
		global_position.x = move_toward(global_position.x, destx, movex)
	$Sprite.play("idle")
	say_random(patrol_lines, patrol_line_time, patrol_line_chance)
	var time = patrol_stop_time
	while time > 0:
		time -= yield()
	var turn_speed = patrol_turn_speed
	if turn_speed > 0:
		$Sprite/LookRay.enabled = false
		var destscalex = -$Sprite.scale.x / abs($Sprite.scale.x)
		while $Sprite.scale.x != destscalex:
			var turnx = yield()*turn_speed
			$Sprite.scale.x = move_toward($Sprite.scale.x, destscalex, turnx)
		$Sprite/LookRay.enabled = !blind

const FLEE_LINES = [
	"AAAAAAA!!!!",
	"THIIIEF!!!!",
	"HEEEELP!!!!"
]

func flee():
	var door = look_around_for(Door.PREV_MASK_BIT)
	if not door is Door:
		queue_free()
		return
	var enter_door_speed = alert_enter_door_speed
	while $Sprite.modulate.a < 1:
		$Sprite.modulate.a = move_toward($Sprite.modulate.a, 1, yield()*enter_door_speed)
	alert_tree(get_parent())
	say_random(FLEE_LINES, 1.0, 1.0, Color.yellow)
	$Sprite.speed_scale = 2
	$Sprite.play("move")
	var destx = door.global_position.x
	while global_position.x != destx:
		global_position.x = move_toward(global_position.x, destx, yield()*alert_speed)
	while $Sprite.modulate.a > 0:
		$Sprite.modulate.a = move_toward($Sprite.modulate.a, 0, yield()*enter_door_speed)
	door.enter(self)

func go_to_door(dest_door:Door):
	var dest_room = dest_door.get_parent()
	get_parent().remove_child(self)
	dest_room.add_child(self)
	global_position = dest_door.global_position
	last_dest_door = dest_door

const CHASE_LINES = [
	"STOP RIGHT THERE!",
	"STOP!!! THIEF!!!!"
]

func player_entered_door(player, door):
	print("player_entered_door", player, door)

# guard cases:
# - go to player ahead of you
# - turn to face player who jumps over you
# - go to door you saw player enter
# - go door to door in search for player
func chase():
	$Sprite.speed_scale = 2
	var speed = alert_speed
	var turn_speed = alert_turn_speed
	var enter_door_speed = alert_enter_door_speed

	while $Sprite.modulate.a < 1:
		$Sprite.modulate.a = move_toward($Sprite.modulate.a, 1, yield()*enter_door_speed)
	alert_tree(get_parent())

	# LOOK AROUND FOR PLAYER
	var lookray:RayCast2D = $Sprite/LookRay
	lookray.enabled = true
	var target
	var destscalex = $Sprite.scale.x / abs($Sprite.scale.x)
	for i in [0, 1]:
		while $Sprite.scale.x != destscalex:
			var turnx = yield()*turn_speed
			$Sprite.scale.x = move_toward($Sprite.scale.x, destscalex, turnx)
		lookray.collision_mask = Player.MASK_BIT
		lookray.force_raycast_update()
		var player = lookray.get_collider() as Player
		if player:
			target = player
			say_random(CHASE_LINES, 1.0, 1.0, Color.red)
			break
		destscalex = -destscalex

	# LOOK_AROUND_FOR_DOOR
	if not target:
		for i in [0, 1]:
			while $Sprite.scale.x != destscalex:
				var turnx = yield()*turn_speed
				$Sprite.scale.x = move_toward($Sprite.scale.x, destscalex, turnx)
			lookray.collision_mask = Door.PREV_MASK_BIT | Door.NEXT_MASK_BIT
			if last_dest_door is Door:
				lookray.add_exception(last_dest_door)
			lookray.force_raycast_update()
			var door = lookray.get_collider() as Door
			if last_dest_door is Door:
				lookray.remove_exception(last_dest_door)
			if door:
				target = door
				break
			destscalex = -destscalex
		last_dest_door = null

	lookray.collision_mask = Player.MASK_BIT
	while target is Player or target is Door:
		var diff_x = target.position.x - position.x
		$Sprite.play("idle" if diff_x == 0 else "move")
		if abs(diff_x) < 1:
			if target is Player:
				if abs(target.position.y - position.y) < 1 and !target.get("won"):
					 # TODO game over
					say("GOTCHA!!!", 1.0, Color.red)
					get_tree().reload_current_scene()
					return
			elif target is Door:
				while $Sprite.modulate.a > 0:
					$Sprite.modulate.a = move_toward($Sprite.modulate.a, 0, yield()*enter_door_speed)
				target.enter(self)
				return
		else:
			destscalex = diff_x / abs(diff_x)
			while $Sprite.scale.x != destscalex:
				var turnx = yield()*turn_speed
				$Sprite.scale.x = move_toward($Sprite.scale.x, destscalex, turnx)
		if target.get_parent() != get_parent(): # PLAYER LEFT THE ROOM
			target = target.get("last_door_entered")
		else:
			position.x = move_toward(position.x, target.position.x, yield()*speed)
			if not target is Player:
				var player = lookray.get_collider() as Player
				if player:
					target = player
					say_random(CHASE_LINES, 1.0, 1.0, Color.red)

func get_seen_player():
	if $Sprite/LookRay.enabled:
		return $Sprite/LookRay.get_collider() as Player

func look_around_for(mask_bit):
	var lookray = $Sprite/LookRay
	lookray.enabled = true
	lookray.collision_mask = mask_bit
	for i in [-1,1]:
		$Sprite.scale.x = i
		lookray.force_raycast_update()
		var object = lookray.get_collider()
		if object:
			return object
	return null

func alert_tree(node):
	var npc = node as NPC
	if npc:
		npc.alert()
	else:
		for child in node.get_children():
			alert_tree(child)

func _physics_process(delta):
	var player = get_seen_player()
	if player and (player.thief_mode or human_alert) and not alerted:
		alert()

	if routine is GDScriptFunctionState and routine.is_valid():
		routine = routine.resume(delta)
	else:
		start_behavior(behavior)

func start_behavior(b):
	behavior = b
	var behavior_name = BEHAVIOR_NAMES[behavior]
	routine = call(behavior_name)

func alert():
	if not alerted:
		alerted = true
		$AudioStreamPlayer2D.play()
		start_behavior(alert_behavior)

func is_now_interactable():
	return not alerted and (pickpocket_value > 0 or pickpocket_key)

func get_interaction_name():
	return "pickpocket"

func pickpocket(pickpocketer):
	if pickpocketer.has_method("take_key"):
		pickpocketer.take_key()
		pickpocket_key = false
	if pickpocketer.has_method("add_money"):
		pickpocketer.add_money(pickpocket_value)
		pickpocket_value = 0

func add_money(money):
	pickpocket_value += money
	var popup = PopupFactory.instance(get_parent())
	popup.global_position = $Top.global_position
	popup.velocity.y = -30
	popup.set_popup("$"+str(money), 1.0, Color.green)

func say(text, time = 1.0, color = Color.white):
	var popup = PopupFactory.instance(self)
	popup.position = $Top.position
	popup.set_popup(text, time, color)
