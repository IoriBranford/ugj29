extends Node

func instance(parent = null):
	var popup = preload("res://Popup.tscn").instance()
	if parent:
		parent.add_child(popup)
	return popup
