extends Area2D
class_name Interactable

const THEFT_ACTIONS = {
	"pickpocket":true,
	"steal":true
}

func is_now_interactable():
	return false

func get_interaction_name():
	return "interact"

func interaction_is_theft():
	var interaction = get_interaction_name()
	return interaction != null and THEFT_ACTIONS.has(interaction)

func interact(user):
	pass

func Interactable_ready():
	var interaction = get_interaction_name()
	if interaction:
		assert(has_method(interaction))
	connect("body_entered", self, "Interactable_on_body_entered")
	connect("body_exited", self, "Interactable_on_body_exited")

func Interactable_on_body_entered(body):
	if body.name == "Player":
		body.call("add_interactable", self)

func Interactable_on_body_exited(body):
	if body.name == "Player":
		body.call("remove_interactable", self)
